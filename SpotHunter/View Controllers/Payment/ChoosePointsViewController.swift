//
//  ChoosePointsViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 7/14/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import Braintree
import BraintreeDropIn
import Alamofire
class ChoosePointsViewController: UIViewController {

    @IBOutlet weak var paymentOption: UIButton!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var price3: UILabel!
    @IBOutlet weak var price2: UILabel!
    @IBOutlet weak var price1: UILabel!
    @IBOutlet weak var point3: UILabel!
    @IBOutlet weak var point2: UILabel!
    @IBOutlet weak var point1: UILabel!
    var price = 0.0
    var points = 0
    var didSelectApplePay = false
    var clientToken = ""
    var braintreeClient: BTAPIClient?
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap1(_:)))
        view1.addGestureRecognizer(tap1)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap2(_:)))
        view2.addGestureRecognizer(tap2)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap3(_:)))
        view3.addGestureRecognizer(tap3)
        fetchClientToken()
        // Do any additional setup after loading the view.
    }
    func fetchClientToken() {
        // TODO: Switch this URL to your own authenticated API
        let url = "http://3.13.67.42:1342/api/get_token"
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            print("Response in login is: ",response)
            switch response.result {
            case let .success(value):
                if let dict = value as? NSDictionary{
                    self.clientToken = dict["clientToken"] as! String
                    self.braintreeClient = BTAPIClient(authorization: self.clientToken);
                            }else{
                                self.showErrorAlert(title: "Failure", message: "Something Went Wrong")
                            }
            case let .failure(error):
                print(error)
            }
            
        })
    }
    @IBAction func topUpTapped(_ sender: UIButton) {
        postNonceToServer(paymentMethodNonce: "visa")
    }
    func postNonceToServer(paymentMethodNonce: String) {
        // Update URL with your server
        let paymentURL = URL(string: "http://3.13.67.42:1342/api/pay1")!
        var request = URLRequest(url: paymentURL)
        request.httpBody = "payment_method_nonce=\(paymentMethodNonce)".data(using: String.Encoding.utf8)
        request.httpMethod = "POST"

        URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
            // TODO: Handle success or failure
            print(response.debugDescription)
        }.resume()
    }
    @IBAction func paymentOption(_ sender: UIButton) {
        self.showDropIn(clientTokenOrTokenizationKey: clientToken);
    }
    @objc func handleTap1(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        view1.backgroundColor = UIColor(red: 63/255, green: 86/255, blue: 255/255, alpha: 1.0)
        view2.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        view3.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        point1.textColor = UIColor.white
        price1.textColor = UIColor.white
        point2.textColor = UIColor.black
        price2.textColor = UIColor.black
        point3.textColor = UIColor.black
        price3.textColor = UIColor.black
        points = 1
        price = 0.99
    }

    @objc func handleTap2(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        view2.backgroundColor = UIColor(red: 63/255, green: 86/255, blue: 255/255, alpha: 1.0)
        view1.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        view3.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        point2.textColor = UIColor.white
        price2.textColor = UIColor.white
        point1.textColor = UIColor.black
        price1.textColor = UIColor.black
        point3.textColor = UIColor.black
        price3.textColor = UIColor.black
        points = 5
        price = 4.95
    }
    @objc func handleTap3(_ sender: UITapGestureRecognizer? = nil) {
        view3.backgroundColor = UIColor(red: 63/255, green: 86/255, blue: 255/255, alpha: 1.0)
        view2.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        view1.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        point3.textColor = UIColor.white
        price3.textColor = UIColor.white
        point2.textColor = UIColor.black
        price2.textColor = UIColor.black
        point1.textColor = UIColor.black
        price1.textColor = UIColor.black
        points = 10
        price = 9.90
        // handling code
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                // Use the BTDropInResult properties to update your UI
                let selectedPaymentOptionType = result.paymentOptionType
                let selectedPaymentMethod = result.paymentMethod
                let selectedPaymentMethodIcon = result.paymentIcon
                let selectedPaymentMethodDescription = result.paymentDescription
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    func showErrorAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

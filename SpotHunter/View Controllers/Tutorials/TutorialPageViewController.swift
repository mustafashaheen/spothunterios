//
//  TutorialPageViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/2/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit

class TutorialPageViewController: UIPageViewController {
        fileprivate lazy var pages: [UIViewController] = {
            return [
                self.getViewController(withIdentifier: "tutorialPageOne"),
                self.getViewController(withIdentifier: "tutorialPageTwo"),
                self.getViewController(withIdentifier: "tutorialPageThree"),
                self.getViewController(withIdentifier: "tutorialPageFour")
            ]
        }()
        
        fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController
        {
            return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
        }
        
        override func viewDidLoad()
        {
            super.viewDidLoad()
            self.dataSource = self
            self.delegate   = self
            
            if let firstVC = pages.first
            {
                setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
            }
        }
    }

    extension TutorialPageViewController: UIPageViewControllerDataSource
    {
        func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
            
            guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
            
            var previousIndex = viewControllerIndex - 1
            
            if(previousIndex < 0)
            {
                return nil
            }
            
            return pages[previousIndex]
        }
        
        func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
        {
            guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
            
            var nextIndex = viewControllerIndex + 1
            
            if(nextIndex > 3){
                return nil
            }
            
            return pages[nextIndex]
        }
    }

    extension TutorialPageViewController: UIPageViewControllerDelegate { }

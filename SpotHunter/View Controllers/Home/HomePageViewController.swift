//
//  HomePageViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/3/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import GoogleMaps
import SWRevealViewController
import Alamofire
class HomePageViewController: UIViewController {

    @IBOutlet weak var parkingView: UIView!
    @IBOutlet weak var menu: UIBarButtonItem!
    let locationManager = CLLocationManager()
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        parkingView.roundedView()
        menu.image = UIImage(named: "sidemenuicon")?.withRenderingMode(.alwaysOriginal)
        self.navigationItem.setHidesBackButton(true, animated: true)
        var token = UserDefaults.standard.value(forKey: "token") as! String
        token = "bearer " + token
        let header: HTTPHeaders = ["Content-Type": "application/json", "Authorization": token]
        let url = baseURL + "getprofile1"
        AF.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: { (response) in
            print(response)
            DispatchQueue.main.async {
                switch response.result {
                case let .success(value):
                    if let dict = value as? NSDictionary{
                        let status = dict["status"] as! Bool
                        if(status == true)
                        {
                            let data = dict["data"] as! NSDictionary
                            let user = data["user"] as! NSDictionary
                            let email = user["email"] as! String
                            let firstName = user["firstName"] as! String
                            let lastName = user["lastName"] as! String
                            let name = firstName + " " + lastName
                            UserDefaults.standard.set(name, forKey: "name")
                            UserDefaults.standard.set(email, forKey: "email")
                        }else{
                            let message = dict["message"] as! String
                            self.showErrorAlert(title: "ERROR", message: message)
                            
                        }
                    }else{
                        self.showErrorAlert(title: "Failure", message: "Something Went Wrong")
                    }
                case let .failure(error):
                    print(error)
                }
                
            }
            
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuPressed(_ sender: UIBarButtonItem) {
        self.revealViewController()?.revealToggle(self)
    }
    func showErrorAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

}
extension HomePageViewController: CLLocationManagerDelegate {
  // 2
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    // 3
    guard status == .authorizedWhenInUse else {
      return
    }
    // 4
    locationManager.startUpdatingLocation()
      
    //5
    mapView.isMyLocationEnabled = true
    //mapView.settings.myLocationButton = true
  }
  
  // 6
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }
      
    // 7
    mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
      
    // 8
    locationManager.stopUpdatingLocation()
  }
}
extension UIView{
    func roundedView(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
            byRoundingCorners: [.topRight,.topLeft],
            cornerRadii: CGSize(width: 25, height: 25))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}

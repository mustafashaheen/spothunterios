//
//  SignUpViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/3/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import Alamofire
class SignUpViewController: UIViewController {

    @IBOutlet weak var referralCode: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func signup(_ sender: UIButton) {
        if((email.text!.isEmpty) || (password.text!.isEmpty) || (confirmPassword.text!.isEmpty) || (firstName.text!.isEmpty) || (lastName.text!.isEmpty) || (phone.text!.isEmpty)) {
            showErrorAlert(title: "ERROR", message: "Please fill all fields")
        }else if(!(isValidEmail(email.text!))) {
            showErrorAlert(title: "ERROR", message: "Please enter valid email address")
        }else if(password.text != confirmPassword.text){
            showErrorAlert(title: "ERROR", message: "Password fields don't match")
        }else{
            let url = baseURL + "user/signup"
            let parameters = [
                "email": email.text,
                "password": password.text,
                "firstName": firstName.text,
                "lastName": lastName.text,
                "phoneNumber": phone.text,
                "referralCode": referralCode.text
                ] as [String: Any]
            AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                print("Response in login is: ",response)
                switch response.result {
                case let .success(value):
                    if let dict = value as? NSDictionary{
                                    let status = dict["status"] as! Bool
                                    if(status == true)
                                    {
                                        let data = dict["data"] as! NSDictionary
                                        let token = data["token"] as! String
                                        UserDefaults.standard.set(token, forKey: "token")
                                        UserDefaults.standard.set(self.phone.text, forKey: "phonenumber")
                                        self.performSegue(withIdentifier: "signupToVerification", sender: self)

                                    }else{
                                        let message = dict["message"] as! String
                                        self.showErrorAlert(title: "ERROR", message: message)
                                        
                                    }
                                }else{
                                    self.showErrorAlert(title: "Failure", message: "Something Went Wrong")
                                }
                case let .failure(error):
                    print(error)
                }
                
            })
        }
    }
    

    func showErrorAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "signupToVerification") {
    var mainController = segue.destination as! PhoneVerificationViewController
        mainController.email = email.text!
    }
    }
}

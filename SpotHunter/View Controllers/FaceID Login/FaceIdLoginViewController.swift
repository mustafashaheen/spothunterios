//
//  FaceIdLoginViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/4/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import LocalAuthentication
class FaceIdLoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if(UserDefaults.standard.value(forKey: "token") != nil){
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [weak self] success, authenticationError in

                DispatchQueue.main.async {
                    if success {
                        if(UserDefaults.standard.value(forKey: "phone") != nil){
                        self?.performSegue(withIdentifier: "faceIdToHome", sender: self)
                        }else{
                            self?.performSegue(withIdentifier: "faceIdToVerification", sender: self)
                        }
                    } else {
                        self!.showErrorAlert(title: "Authentication failed", message: "You could not be verified; please try again.")
                    }
                }
            }
        } else {
            self.showErrorAlert(title: "Biometry Unavailable", message: "Your device is not configured for biometric authentication.")
        }
        }else{
            self.showErrorAlert(title: "No User Found", message: "Please Login using your email address first")
        }
    }
    func showErrorAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "faceIdToHome") {
    var mainController = segue.destination
    } else if (segue.identifier == "faceIdToVerification") {
    var mainController = segue.destination as! PhoneVerificationViewController
    }
    }

}

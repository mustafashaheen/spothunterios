//
//  ShareParkingViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/11/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
class ShareParkingViewController: UIViewController {

    @IBOutlet weak var parkingView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var information: UITextView!
    @IBOutlet weak var metered: UIButton!
    @IBOutlet weak var hourly: UIButton!
    var meter = false
    var hour = false
    var position = CLLocationCoordinate2D()
    override func viewDidLoad() {
        super.viewDidLoad()
        parkingView.roundedView()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        mapView.camera = GMSCameraPosition(target: position, zoom: 15, bearing: 0, viewingAngle: 0)
        let marker = GMSMarker(position: position)
        marker.title = "Pin Exact Location"
        marker.icon = UIImage(named: "Pin")
        marker.map = mapView
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        let nav = self.navigationController //grab an instance of the current navigationController
        DispatchQueue.main.async { //make sure all UI updates are on the main thread.
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "chooseLocation") as! ChooseLocationViewController
            obj.navigationItem.setHidesBackButton(true, animated: true)
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(obj, animated: false)
        }
    }
    
    @IBAction func meteredPressed(_ sender: UIButton) {
        metered.backgroundColor = UIColor(red: 63/255, green: 86/255, blue: 255/255, alpha: 1)
        metered.setTitleColor(UIColor.white, for: .normal)
        hourly.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        hourly.setTitleColor(UIColor.black, for: .normal)
        meter = true
        hour = false
    }
    
    @IBAction func hourlyPressed(_ sender: UIButton) {
        hourly.backgroundColor = UIColor(red: 63/255, green: 86/255, blue: 255/255, alpha: 1)
        hourly.setTitleColor(UIColor.white, for: .normal)
        metered.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        metered.setTitleColor(UIColor.black, for: .normal)
        meter = false
        hour = true
    }
    
    @IBAction func shareParking(_ sender: UIButton) {
        if((hour == false) && (meter == false)){
            self.showErrorAlert(title: "ERROR", message: "Please select parking type")
        }else if(information.text.isEmpty){
            self.showErrorAlert(title: "ERROR", message: "Please add the information")
        }else{
            var type = ""
            if(hour){
                type = "hourly"
            }else{
                type = "metered"
            }
            
            var token = UserDefaults.standard.value(forKey: "token") as! String
            token = "bearer " + token
            let header: HTTPHeaders = ["Content-Type": "application/json", "Authorization": token]
            let url = baseURL + "createspot1"
            let parameters = [
                "latitude": self.position.latitude,
                "longitude": self.position.longitude,
                "type": type,
                "description": self.information.text
            ] as [String: Any]
            AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: { (response) in
                print(response)
                DispatchQueue.main.async {
                    switch response.result {
                    case let .success(value):
                        if let dict = value as? NSDictionary{
                            let status = dict["status"] as! Bool
                            if(status == true)
                            {
                                self.performSegue(withIdentifier: "shareParkingToCongratulations", sender: self)
                            }else{
                                let message = dict["message"] as! String
                                self.showErrorAlert(title: "ERROR", message: message)
                                
                            }
                        }else{
                            self.showErrorAlert(title: "Failure", message: "Something Went Wrong")
                        }
                    case let .failure(error):
                        print(error)
                    }
                    
                }
                
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "shareParkingToCongratulations") {
    var mainController = segue.destination as! CongratulationsViewController
        mainController.fromShare = true
    }
    }
    func showErrorAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

//
//  PhoneVerificationViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/4/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import Alamofire
class PhoneVerificationViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var four: UITextField!
    @IBOutlet weak var three: UITextField!
    @IBOutlet weak var two: UITextField!
    @IBOutlet weak var one: UITextField!
    var email = ""
    var token = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        if(UserDefaults.standard.value(forKey: "token") != nil){
            token = UserDefaults.standard.value(forKey: "token") as! String
            token = "bearer " + token
        }
        if(UserDefaults.standard.value(forKey: "phonenumber") != nil){
            let phoneNumber = UserDefaults.standard.value(forKey: "phonenumber") as! String
            heading.text = "Please enter the 4-digit code sent to " + phoneNumber
        }else{
            heading.text = "Please enter the 4-digit code sent to your phone number."
        }
        one.delegate = self
        two.delegate = self
        three.delegate = self
        four.delegate = self
        // Do any additional setup after loading the view.
    }
    @IBAction func resend(_ sender: UIButton) {
        let header: HTTPHeaders = ["Content-Type": "application/json", "Authorization": token]
        let url = baseURL + "resend1"
        AF.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: { (response) in
            print("Response in verification is: ",response)
            switch response.result {
            case let .success(value):
                if let dict = value as? NSDictionary{
                                let status = dict["status"] as! Bool
                                if(status == true)
                                {
                                    self.showErrorAlert(title: "SUCESS", message: "Verification code has been sent to your phone number")

                                }else{
                                    let message = dict["message"] as! String
                                    self.showErrorAlert(title: "ERROR", message: message)
                                    
                                }
                            }else{
                                self.showErrorAlert(title: "Failure", message: "Something Went Wrong")
                            }
            case let .failure(error):
                print(error)
            }
            
        })
        
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        
        if((one.text!.isEmpty) || (two.text!.isEmpty) || (three.text!.isEmpty) || (four.text!.isEmpty)){
            showErrorAlert(title: "ERROR", message: "Please fill all fields")
        }else{
            let header: HTTPHeaders = ["Content-Type": "application/json", "Authorization": token]
            var code = one.text! + two.text! + three.text!
            code += four.text!
        let url = baseURL + "verify1"
        let parameters = [
            "code": code
            ] as [String: Any]
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: { (response) in
            print("Response in verification is: ",response)
            switch response.result {
            case let .success(value):
                if let dict = value as? NSDictionary{
                                let status = dict["status"] as! Bool
                                if(status == true)
                                {
                                    UserDefaults.standard.set(true, forKey: "phone")
                                
                                    self.performSegue(withIdentifier: "verificationToCongratulations", sender: self)

                                }else{
                                    let message = dict["message"] as! String
                                    self.showErrorAlert(title: "ERROR", message: message)
                                    
                                }
                            }else{
                                self.showErrorAlert(title: "Failure", message: "Something Went Wrong")
                            }
            case let .failure(error):
                print(error)
            }
            
        })
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                           replacementString string: String) -> Bool
    {
        
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString

        if(newString.length == maxLength){
            if(textField == one){
                one.text = newString as String
                two.becomeFirstResponder()
            }
            if(textField == two){
                two.text = newString as String
                three.becomeFirstResponder()
            }
            if(textField == three){
                three.text = newString as String
                four.becomeFirstResponder()
            }
        }
        return newString.length <= maxLength
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor(red: 63/255, green: 86/255, blue: 255/255, alpha: 1.0).cgColor
    }
    func showErrorAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "verificationToCongratulations") {
    var mainController = segue.destination as! CongratulationsViewController
    }
    }
}

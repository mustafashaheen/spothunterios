//
//  CongratulationsViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/4/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import SWRevealViewController
class CongratulationsViewController: UIViewController {

    @IBOutlet weak var points: UILabel!
    var fromShare = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if(fromShare == true){
            points.text = "You have earned 1 Point by sharing a spot."
        }

        self.navigationItem.setHidesBackButton(true, animated: true)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backHomePressed(_ sender: UIButton) {
        let nav = self.navigationController //grab an instance of the current navigationController
        DispatchQueue.main.async { //make sure all UI updates are on the main thread.
            if(self.fromShare){
                nav?.dismiss(animated: true, completion: nil)
            }else{
                self.performSegue(withIdentifier: "congratulationsToReveal", sender: self)
            }
            
 
        }
    }
    

}

//
//  Functions.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/3/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import Foundation


func isValidEmail(_ testStr: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

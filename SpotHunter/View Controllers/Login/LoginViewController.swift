//
//  LoginViewController.swift
//  SpotHunter
//
//  Created by Mustafa Shaheen on 6/3/20.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import Alamofire
class LoginViewController: UIViewController {

    @IBOutlet weak var signup: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        signup.backgroundColor = .clear
        signup.layer.cornerRadius = 5
        signup.layer.borderWidth = 2
        signup.layer.borderColor = UIColor(red: 63/255, green: 86/255, blue: 255/255, alpha: 1.0).cgColor
        UserDefaults.standard.set(true, forKey: "login")
        // Do any additional setup after loading the view.
    }
    

    @IBAction func loginPressed(_ sender: UIButton) {
        if((email.text!.isEmpty) || (password.text!.isEmpty))
        {
            showErrorAlert(title: "ERROR", message: "Please fill all fields")
        } else if(!(isValidEmail(email.text!)))
        {
            showErrorAlert(title: "ERROR", message: "Please enter valid email address")
        }else{
                    let url = baseURL + "user/login"
                    let parameters = [
                        "email": email.text,
                        "password": password.text
                        ] as [String: Any]
                    AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                        print("Response in login is: ",response)
                        switch response.result {
                        case let .success(value):
                            if let dict = value as? NSDictionary{
                                            let status = dict["status"] as! Bool
                                            if(status == true)
                                            {
                                                let data = dict["data"] as! NSDictionary
                                                let user = data["user"] as! NSDictionary
                                                let token = data["token"] as! String
                                                UserDefaults.standard.set(token, forKey: "token")
                                                let verify = user["verify"] as! Int
                                                if(verify != 1){
                                                    self.performSegue(withIdentifier: "loginToVerification", sender: self)
                                                }else{
                                                    UserDefaults.standard.set(true, forKey: "phone")
                                                self.performSegue(withIdentifier: "loginToHome", sender: self)
                                                }

                                            }else{
                                                let message = dict["message"] as! String
                                                self.showErrorAlert(title: "ERROR", message: message)
                                                
                                            }
                                        }else{
                                            self.showErrorAlert(title: "Failure", message: "Something Went Wrong")
                                        }
                        case let .failure(error):
                            print(error)
                        }
                        
                    })
        }
    }
    
    func showErrorAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "loginToHome") {
    var mainController = segue.destination
    }else if(segue.identifier == "loginToVerification"){
        var mainController = segue.destination as! PhoneVerificationViewController
        mainController.email = self.email.text!
        }
    }
}
